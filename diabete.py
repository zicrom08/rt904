import numpy as np
import pandas as pd
import sklearn
from sklearn import tree
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

#Lecture des donnees
df = pd.read_csv("./diabetes-1.csv")

print()
#Verification des infos
for c in df.columns:
    print('For column', c, 'there are', df[df[c] == 0][c].count(),'zero values.')

print()
#Visualisation de donnees : supposition que l'age est significatif dans la présence de diabete
df['PredictedOutcome']=np.where(df.Age<30,0,1)
N_correct=df[df.PredictedOutcome==df.Outcome].shape[0]
N_total=df.shape[0]
accuracy=N_correct/N_total
print('Nombre d\'exemples corrects = ', N_correct)
print('Nombre d\'exemples total = ', N_total)
print('Precision de l\'Age = ', accuracy)
print()

#Visualisation de donnees : supposition que le glucose est significatif dans la presence de diabete
df['PredictedOutcome']=np.where(df.Glucose<125,0,1)
N_correct=df[df.PredictedOutcome==df.Outcome].shape[0]
N_total=df.shape[0]
accuracy=N_correct/N_total
print('Nombre d\'exemples corrects = ', N_correct)
print('Nombre d\'exemples total = ', N_total)
print('Precision par le Glucose = ', accuracy)
print()

#Preparation des donnees
donnees = df.drop('Insulin',axis=1,inplace=False)

train, test = train_test_split(donnees, test_size = 0.3, random_state = 0)

def imputeColumns(dataset):
    columnsToImpute=['Glucose', 'BloodPressure', 'SkinThickness', 'BMI']
    for c in columnsToImpute:
        avgOfCol=dataset[dataset[c]>0][[c]].mean()
        dataset[c+'_imputed']=np.where(dataset[[c]]!=0,dataset[[c]],avgOfCol)

imputeColumns(train)
imputeColumns(test)

X_train = train[['Pregnancies', 'Glucose_imputed', 'BloodPressure_imputed', 'SkinThickness_imputed', 'BMI_imputed', 'DiabetesPedigreeFunction', 'Age']]
Y_train = train[['Outcome']]
X_test = test[['Pregnancies','Glucose_imputed', 'BloodPressure_imputed','SkinThickness_imputed','BMI_imputed','DiabetesPedigreeFunction','Age']]
Y_test = test[['Outcome']]

#Classification avec les arbres de decision
arbre_de_decision = tree.DecisionTreeClassifier(random_state = 0)
arbre_de_decision.fit(X_train, Y_train)
print()
print("Pourcentage de precision via un arbre de decision : ", arbre_de_decision.score(X_test, Y_test)*100,"%")
print()
resultats_decision_test = arbre_de_decision.predict(X_test)
tableau_Y_test = Y_test.reset_index(level=None,inplace=False)

VV = 0
VF = 0
FV = 0
FF = 0

for i in range(0, resultats_decision_test.shape[0]):
    if resultats_decision_test[i]==1 and tableau_Y_test.Outcome[i]==1:
        VV = VV + 1
    elif resultats_decision_test[i]==1 and tableau_Y_test.Outcome[i]==0:
        VF = VF + 1
    elif resultats_decision_test[i]==0 and tableau_Y_test.Outcome[i]==1:
        FV = FV + 1
    else:
        FF = FF + 1

print('Pourcentage de (VV) : ', (VV/resultats_decision_test.shape[0])*100,"%")
print('Pourcentage de (VF) : ', (VF/resultats_decision_test.shape[0])*100,"%")
print('Pourcentage de (FV) : ', (FV/resultats_decision_test.shape[0])*100,"%")
print('Pourcentage de (FF) : ', (FF/resultats_decision_test.shape[0])*100,"%")
print()
#Méthode d'approche avec KNN
knn = KNeighborsRegressor(n_neighbors=5)
knn.fit(X_train, Y_train)
predictions = knn.predict(X_test)
erreur = (((predictions - Y_test)**2).sum())/len(predictions)
print("Pourcentage de precision de la methode KNN : ", (1-erreur.Outcome)*100,"%")
